# Go4Debian projet

Website : http://go4debian.tuxfamily.org/

Wiki : https://git.framasoft.org/cid/Go4Debian/wikis/home

The main repository is hosted on tuxfamily.

## Features

Currently, this repository provides :

- [X] 8757 commented games in sgf format from gtl.xmp.net
- [X] 7319 professional games in sgf format
- [X] 14 games in sgf format from Hikaru No Go
- [X] The "Kogo's Joseki" dictionary
- [X] Some free rights pdfs

## Soon...

- [ ] Fuseki database from [Arno](http://xmp.net/arno/fuseki.html)
- [ ] ```sfgutils```

## Installation

```shell
sudo echo "deb http://download.tuxfamily.org/go4debian all/" >> /etc/apt/sources.list
sudo apt-get update
```

It's an non signed repository, you should have some warnings during update.
Files are installed in ```/var/games/go/```

### Install all-in packages

```shell
sudo apt-get install go4debian
```

### Install individual packages

```shell
sudo apt-get install go-books go-sgf-games go-sgf-gtl.xmp.net go-sgf-hikaru go-sgf-kogojoseki
```

## Credits

The commented games are provided by The Go Teaching Ladder.

The joseki dictionary is provided by Masahiko Ohashi.