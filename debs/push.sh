#!/bin/bash
# Simple push to repo

# Get all deb packages
rsync ../packages/*/*.deb all/

# Create Packages.gz
dpkg-scanpackages all | gzip -9c > all/Packages.gz

# Push to tuxfamily
rsync all/* cid@ssh.tuxfamily.org:/home/go4debian/go4debian-repository/all/