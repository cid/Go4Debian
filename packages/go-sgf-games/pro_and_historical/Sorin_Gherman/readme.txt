This is the content of this zip file:

Games played against professional Go players
____________________________________________

 s-satoru.sgf Kobayashi Satoru

 s-tei.sgf Tei Meiko

 s-tei2.sgf Tei Meiko

 s-tei3.sgf Tei Meiko

 s-kosan.sgf Jiang Zhujiu "Jujo"

 s-kosan2.sgf Jiang Zhujiu "Jujo"

 s-konagai.sgf Konagai

 s_kitano1.sgf Kitano Ryo

 s_kitano2.sgf Kitano Ryo

 s-chang.sgf Chang

 s-so.sgf So


Games played against other inseis
_________________________________

 1_s_shimoda.sgf Insei game 1

 2_s_mmoto.sgf Insei game 2

 s_lee_1.sgf Insei game 3

 s_ozawa1_1.sgf Insei game 4

 s-matsuda_1.sgf Insei game 5

 s-turuiama.sgf Insei game 6: we see how important the "Play hane at the head of two stones" 
basic Go proverb is

 lee210394.sgf Insei game 7: shows that professionals don't care about joseki, 
but judge every position as a whole

 insei270394.sgf Insei game 8: when both miss a vital area

 lee270394.sgf Insei game 9: big moyo

 ozawa030494.sgf Insei game 10

 nakazawa100494.sgf Insei game 11

 tsuruyama170494.sgf Insei game 12: very strong opponent

 lee240494.sgf Insei game 13: don't play slack when you are behind, or else...

 ozawa290494.sgf Insei game 14
